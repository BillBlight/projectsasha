<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
include 'authconfig.php';
$msgformat = "%s, the GDPR Watchdog says you have not been authorized. (Right Click) Open %s to authorize this grid.";
$failmsg = "Authentication has failed, please try again or from another region, if the error persists contact the grid admin";

class AuthorizationRequest
    {
    	private $m_isAuthorized;
    	private $m_message;    	
    	public $ID;
    	public $FirstName;
    	public $SurName;
    	public $Email;
    	public $RegionName;
    	public $RegionID;    	
 
    	public function parseRequest($request)
    	{
    		$reader = new XMLReader();
 
                $reader->XML($request);
			while ($reader->read()) 
			{
      			if ($reader->nodeType == XMLReader::ELEMENT) 
      			{
      				switch($reader->name)
      				{
      					case 'AuthorizationRequest':
      						//$log->write("AuthorizationRequest element");
      					break;	
      					case 'ID':
      						$reader->read();
      						$this->ID = $reader->value;
      					break;
      					case 'FirstName':
      						$reader->read();
      						$this->FirstName = $reader->value;
      					break;
      					case 'SurName':
      						$reader->read();
      						$this->SurName = $reader->value;
      					break;
      					case 'Email':
      						$reader->read();
      						$this->Email = $reader->value;
      					break;
      					case 'RegionName':
      						$reader->read();
      						$this->RegionName = $reader->value;
      					break;
      					case 'RegionID':
      						$reader->read();
      						$this->RegionID = $reader->value;
      					break;
      				}
 
      			}
 
			}
    	}	
 
    }
	
class AuthorizationResponse
    {
    	private $m_isAuthorized;
    	private $m_message;
 
    	public function AuthorizationResponse($isAuthorized,$message)
    	{
    		$this->m_isAuthorized = $isAuthorized;
    		$this->m_message = $message;
    	}
 
    	public function toXML()
    	{
    		return '<?xml version="1.0" encoding="utf-8"?><AuthorizationResponse xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><IsAuthorized>'. $this->m_isAuthorized .'</IsAuthorized><Message><![CDATA['. $this->m_message .']]></Message></AuthorizationResponse>';
 
    	}
    }

	$request = @file_get_contents('php://input');
	
	//$xml = simplexml_load_file('php://input');
	
$xml3 = simplexml_load_string($request);
$uuid = $xml3->ID;
$firstname = $xml3->FirstName;
$lastname = $xml3->SurName;
$regionuuid = $xml3->RegionID;
$regionname = $xml3->RegionName;


$authReq = new AuthorizationRequest();
$authReq->parseRequest($request);


	

$nametest = substr($lastname,0,1);
$avatarname	= $firstname.$lastname;

$authlink2 = $authlink . "firstname=" . $firstname . "&lastname=" . $lastname . "&uuid=" . $uuid;
$query2 = $db->prepare("
            SELECT *
            FROM $tablename
            WHERE avatarname LIKE :avatarname");
$query2->bindParam(':avatarname', $avatarname);
$query2->execute(); 
$no = $query2->rowCount();

if($nametest == "@" && $no > 0 )
{


$authResp = new AuthorizationResponse("true","Authorized");
    echo $authResp->toXML();
exit();
break;
}


if($nametest == "@" && $no < 1 ) 
		{ 

$newmsg = sprintf($msgformat,$firstname,$authlink2);
$authResp = new AuthorizationResponse("false",$newmsg);
    echo $authResp->toXML();
exit();
break;
		}
		
if($nametest != "@")
{
								
$authResp = new AuthorizationResponse("true","Authorized");
echo $authResp->toXML();
exit();
break;
}

if($nametest != "" )
{
								
$newmsg = $failmsg;
$authResp = new AuthorizationResponse("false",$newmsg);
    echo $authResp->toXML();
exit();
break;
}
				
			
?>