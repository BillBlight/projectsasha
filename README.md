# ProjectSasha
## Project Sasha, GDPR Guard Dog for Opensimulator
### This project is currently in the _"FORGOTTEN"_ stage.

Currently, there is ZERO support for this project.  You are on your own.  
If you know where to find me, other than here, you might have some luck.

### USE AT YOUR OWN RISK, I/WE claim no responisbility if you blow up your grid,
### get attacked by aliens, an asteroid strike occurs, or anything else comes along as a result of using this project, I/WE CLAIM NO RESPONSIBILITY.

### _This code is very messy right now, so yes I know it is sloppy, true spaghetti style._
  
  

## This project aims provide a standalone GDPR _"compliant"_ HG authorization page for OpenSimulator.  


The goal of this project is not to provide a full featured remote authorization service, but to offer a way of only authorizing HG visitors to your grid.  
The project's goal is ONLY to authorize HG visitors to your grid.  It is NOT a full-featured remote authorization service.

This project requires no actual connection to your opensimulator database,  
It is totally standalone.  The goal is to have it ignore local accounts.

There are three files you will need to edit to suit your needs.  The FIRST ONE is the important one:

* authconfig.php, this file has all your database settings and links in it.
* header.php, this is where the meat of the page you present to your user is stored.
* footer.php, the text here is displayed right under the YES/NO buttons.

Included is the hgauth.sql, this file will give you the database structure you need
import it into your mysql server, into the database you create for use.

You need to make two changes to your Grid, (this should also work on connected regions, ie ones connected so grids such as OSgrid)

config-include/GridCommon.ini (Grid Mode)  
or  
config-include/StandaloneCommon.ini (Standalone Mode)  
  
[Modules]  
 AuthorizationServices = "RemoteAuthorizationServicesConnector"

[AuthorizationService]
	
 change this to your grid-wide authorization server

 AuthorizationServerURI = "http://yourwebserver/hgauth/hgauth.php"  
	"it is best if you put it in a sub directory"
	
 
		
	Requirements  
	  * Webserver, tested on Apache 
	  * PHP, tested and developed on PHP 5.6 and 7
	  * mySQL server, tested on MariaDB 10.x (will not work on mySQL prior to 5.6 without manual adjustments, thanks to Andsim for discovering this.)
	  * Currently there are no plans to support anything older than was used for development and testing.		

  
    
	
	
   * Yes this is named after my dog,  
     she crossed the rainbow bridge on 08-31-2018.



* **_Special thanks to Foto50, of the Jopensim Project for some ideas and inspiration from his code._**
[jOpenSim](https://www.jopensim.com/ "jOpensim / jOpensimWorld") ![jopensim](https://www.jopensim.com/images/logos/logo_64.png "jOpenSim") 
* **_Special thanks to Hack13 for looking over it , and his input._**  
* **_Special thanks to FreakyTech for finding my screw ups._** 
* **_Special thanks to Leighton Marjoram for being a tester._**
* **_Special thanks to All my OpenSimLife Family._**  
 
  
  [The Opensimulator Project](http://opensimulator.org/wiki/Main_Page "The Opensimulator Project") ]   
  [Opensimulator AuthorizationService](http://opensimulator.org/wiki/AuthorizationService "Opensimulator AuthorizationService")
    
	  
   
> we make no legal claims that this is by any way GDPR compliant, this is simply a tool to show how you can become compliant.  Please consult your own legal authority on any compliance issues.	
