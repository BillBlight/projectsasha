<style type="text/css">
#agree {
	font-weight: bold;
	font-style: italic;
	text-align: left;
}
.gdprtext {
	text-align: left;
	font-weight: bold;
	font-weight: normal;
}
.retry {
	font-style: italic;
}
.retry {
	text-align: left;
}
@import url(http://fonts.googleapis.com/css?family=Raleway);

#main{
width:960px;
margin:50px auto;
font-family: 'Raleway', sans-serif;
}

h2{
background-color: #FEFFED;
text-align:center;
border-radius: 10px 10px 0 0;
margin: -10px -40px;
padding: 15px;
}

hr{
border:0;
border-bottom:1px solid #ccc;
margin: 10px -40px;
margin-bottom: 30px;
}

#login{
width:300px;
float: left;
border-radius: 10px;
font-family:raleway;
border: 2px solid #ccc;
padding: 10px 40px 25px;
margin-top: 70px;
}

input[type=text],input[type=email]{
width:99.5%;
padding: 10px;
margin-top: 8px;
border: 1px solid #ccc;
padding-left: 5px;
font-size: 16px;
font-family:raleway;
}

input[type=submit]{
	width: 100%;
	background-color: #009900;
	color: white;
	border: 2px solid #00FF00;
	padding: 10px;
	font-size: 20px;
	cursor: pointer;
	border-radius: 5px;
	margin-bottom: -12px;
}
input[type=submit]:hover {
	border: none;
	box-shadow: 0px 0px 1px #777;
	color: #000;
	background-color: #F90;
}

#formget{
float:right;
}
h1 {
margin-left: -85px;
}

body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
</style>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"><p class="gdprtext" style="text-decoration: underline; font-weight: bolder; font-size: 18px;">Authorize HyperGrid Avatar</p>
<p class="gdprtext"><br />
  <span class="gdprtext"><span class="gdprtext">GDP regulations take effect starting on May 25th, 2018.</span></span></p>
<p class="gdprtext"><span class="gdprtext"> This new legislation requires all companies offering services to members of the European Union <br />
to specifically ask for permission to store and use your data. </span></p>
<p class="gdprtext"><span class="gdprtext">There is some data regarding your avatar that is required to be held by our system to maintain function, <br />
  for you and the avatars you may interact with. <br />
  <br />
  Without your permission we cannot allow this data to be transfered to our system, and <br />
therefore must require you to give us this permission. </span></p>
<p class="gdprtext"><span class="gdprtext">Securing,  storing your data, will only be used for the purpose of your visits here and <br />
your interactions with other users, we will not share it with any 3rd party or parties <br />
unless specifically required to do so by law.</span></p>
<p class="gdprtext"><span class="gdprtext">When you autorize your avatar this will authorize and store your,<br />
  <br />
  Avatar First Name<br />
  Avatar Last Name<br />
  Avatar UUID<br />
  Current IP Address<span style="font-size: 9px; font-style: italic;"> (this is stored transiently, unless needed for a security reason)</span></span><br />
  <br />
  <span class="gdprtext"><span class="gdprtext">*NOTE: For transparency, you also should be informed that your Avatar Name and UUID is exchanged between grids and <br />
  grid users, for things
such as , Friendships, Friendship Requests, Instant messages, Profiles, and <br />
inventory exchanges, regardless if you actually travel to a foreign grid.</span><br />
</p>
