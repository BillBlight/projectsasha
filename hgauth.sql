
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";



CREATE TABLE IF NOT EXISTS `hgauth` (
  `uuid` char(36) CHARACTER SET utf8 NOT NULL,
  `avatarname` varchar(64) CHARACTER SET utf8 NOT NULL,
  `confirmtime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`uuid`),
  UNIQUE KEY avatarname (`avatarname`),
  KEY confirmtime (`confirmtime`)
) DEFAULT CHARSET=utf8;
COMMIT;

