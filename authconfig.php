<?php
//test url for me, and for example formating for the authsubmit page
//index.php?firstname=Bill.Blight&lastname=@grid.opensim.life:8002&uuid=00000000-0000-0000-0000-000000000000


//Database settings for your Sasha database.
$dbhost = "serveraddress";
$dbusername = "username";
$dbpassword = "password";
$dbname = "hgauth";
$tablename = "hgauth"; //this is the table name that will store your authorizations.

//some debug info I will remove once it is published


//important URLs and pages
$privacypolicy = "https://yoursite.com/yourprivacypolicy"; //link to your grids full privacy policy
$baseurl = "http://yourserver.com/hgauth/"; //root url for the auth system
$authpage = "index.php?"; //name of the page your users will use to submit consent, if you rename the deafult page please change this.

//joining variables for later use

$authlink = $baseurl . $authpage;

//don't change anything below here unless you know what you are doing.


$dsn = "mysql:host=$dbhost;dbname=$dbname;";

$opt = [PDO::ATTR_PERSISTENT => false];

$db = new PDO($dsn, $dbusername, $dbpassword, $opt);



$query2 = $db->prepare("
            SELECT *
            FROM $tablename
            WHERE avatarname LIKE :avatarname");
$query2->bindParam(':avatarname', $avatarname);
$query1 = $db->prepare("
            SELECT *
            FROM $tablename
            WHERE uuid LIKE :uuid");
$query1->bindParam(':uuid', $uuid);
?>